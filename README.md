# Neo4J repair toolkit #

## Description

This is a Java tool which allows to make a straightforward repair of a Neo4J database by cloning the database with skipping broken parts

## What is this repository for?

### Quick summary
### Version

Latest version is `1.0-SNAPSHOT`

### Similar tools

## How do I get set up?

### Summary of set up
### Quick run

Just grab the .jar file and execute `java -server -jar Neo4jIntegratorMaven.jar` 

### Configuration
### Dependencies

All dependencies are already included in the jar file, and they are defined in the Maven `pom.xml` file

### Database configuration
### How to run tests
### Deployment instructions

## Contribution guidelines

### Writing tests
### Code review
### Other guidelines

## Who do I talk to?

### Repo owner or admin
### Other community or team contact

## List of contributors