package N4jTgdIntegr;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by oskarjarczyk on 30.06.2017.
 */
public enum Constant {

    VERBOSE_STEP,
    VERBOSE_SMALL_STEP,
    VERBOSE;

    private static final String PATH = "/app.properties";

    private static final Logger logger = LoggerFactory.getLogger(Constant.class);

    private static Properties properties;

    private String value;

    private void init() {
        if (properties == null) {
            properties = new Properties();
            try {
                properties.load(Constant.class.getResourceAsStream(PATH));
            } catch (Exception e) {
                logger.error("Unable to load " + PATH + " file from classpath", e);
                System.exit(1);
            }
        }
        value = (String) properties.get(this.toString().toLowerCase().replace('_','.'));
    }

    public String getValue() {
        if (value == null) {
            init();
        }
        return value;
    }

    public Long getLongValue() {
        return Long.parseLong(getValue());
    }
}
