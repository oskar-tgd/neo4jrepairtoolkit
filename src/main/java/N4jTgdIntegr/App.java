package N4jTgdIntegr;

import org.apache.commons.cli.*;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import java.io.File;
import java.io.IOException;


public class App {
    public static void main(String[] args) {
        CommandLine commandLine;
        Option option_left = Option.builder("from")
                .required(false)
                .desc("Location from where data is copied")
                .longOpt("left")
                .build();

        Option option_right = Option.builder("to")
                .required(true)
                .desc("Location to where copy the data")
                .longOpt("right")
                .build();

        Option left_config = Option.builder("Lconfig")
                .required(false)
                .desc("Location of source neo4j.conf file")
                .longOpt("source_config")
                .build();

        Option right_config = Option.builder("Rconfig")
                .required(false)
                .desc("Location of target neo4j.conf file")
                .longOpt("target_config")
                .build();

        Options options = new Options();
        CommandLineParser parser = new DefaultParser();

        options.addOption(option_left);
        options.addOption(option_right);
        options.addOption(left_config);
        options.addOption(right_config);

        String targetPath = "/var/lib/neo4j/data/databases/graph-proper.db";
        String sourcePath = "/var/lib/neo4j/data/databases/graph.db";

        String leftConfigPath = "/etc/neo4j/neo4j.conf";
        String rightConfigPath = "/etc/neo4j/neo4j2nd.conf";

        try {
            commandLine = parser.parse(options, args);

            if (commandLine.hasOption("from"))
            {
                sourcePath = commandLine.getOptionValue("from");
            }
            if (commandLine.hasOption("to"))
            {
                targetPath = commandLine.getOptionValue("to");
            }
            if (commandLine.hasOption("Lconfig"))
            {
                leftConfigPath = commandLine.getOptionValue("Lconfig");
            }
            if (commandLine.hasOption("Rconfig"))
            {
                rightConfigPath = commandLine.getOptionValue("Rconfig");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("Neo4J repair toolkit starts execution");

        try {
            new App().dumpAndLoadTo(sourcePath, leftConfigPath, targetPath, rightConfigPath);
        } catch (InterruptedException e) {
            System.out.println("Ooops! Something went wrong");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IOException detected");
            e.printStackTrace();
        }
        System.out.println("Done working");
    }

    private void dumpAndLoadTo(String sourcePath, String sourceConfigPath, String targetPath, String targetConfigPath) throws IOException, InterruptedException {
        System.out.println("Loading to db " + targetPath);

        long counter = 0;

        System.out.println("Creating source neo4j instance...");
        GraphDatabaseService dbSrc = getDb(sourcePath,
                sourceConfigPath);
        System.out.println("Creating target neo4j instance...");
        GraphDatabaseService dbDest = getDb(targetPath,
                targetConfigPath);

        System.out.println("Starting data integration....");

        Transaction txSource = dbSrc.beginTx();
        Transaction txDest = dbDest.beginTx();

        ResourceIterator<Node> allNodes = dbSrc.getAllNodes().iterator();

        while (allNodes.hasNext()) {

            Node srcNode = allNodes.next();
            counter++;

            if (counter % Constant.VERBOSE_STEP.getLongValue() == 0) {
                txDest.success();
                txDest.close();
                System.out.print('+');
                txDest = dbDest.beginTx();
            }
            if (counter % Constant.VERBOSE_SMALL_STEP.getLongValue() == 0) {
                System.out.print('.');
            }

            Node destNode = dbDest.createNode();

            for (Label srcLabel : srcNode.getLabels())
                destNode.addLabel(srcLabel);
            for (String srcKey : srcNode.getPropertyKeys())
                destNode.setProperty(srcKey, srcNode.getProperty(srcKey));
        }

        System.out.println("Closing all transactions...");

        txDest.success();
        txDest.close();

        txSource.success();
        txSource.close();

        System.out.println("Shutting down...");

        dbSrc.shutdown();
        dbDest.shutdown();
    }

    private GraphDatabaseService getDb(String databasePath, String configPath) {
        GraphDatabaseService result = new GraphDatabaseFactory()
                .newEmbeddedDatabaseBuilder(
                        new File(databasePath))
                .loadPropertiesFromFile(configPath).
                        newGraphDatabase();
        return result;
    }

}
